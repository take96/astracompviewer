# AstraViewerComp CMake config file
#
# This file sets the following variables:
# AstraViewerComp_FOUND - Always TRUE.
# AstraViewerComp_INCLUDE_DIRS - Directories containing the AstraViewerComp include files.
# AstraViewerComp_IDL_DIRS - Directories containing the AstraViewerComp IDL files.
# AstraViewerComp_LIBRARIES - Libraries needed to use AstraViewerComp.
# AstraViewerComp_DEFINITIONS - Compiler flags for AstraViewerComp.
# AstraViewerComp_VERSION - The version of AstraViewerComp found.
# AstraViewerComp_VERSION_MAJOR - The major version of AstraViewerComp found.
# AstraViewerComp_VERSION_MINOR - The minor version of AstraViewerComp found.
# AstraViewerComp_VERSION_REVISION - The revision version of AstraViewerComp found.
# AstraViewerComp_VERSION_CANDIDATE - The candidate version of AstraViewerComp found.

message(STATUS "Found AstraViewerComp-1.0.0")
set(AstraViewerComp_FOUND TRUE)

find_package(<dependency> REQUIRED)

#set(AstraViewerComp_INCLUDE_DIRS
#    "C:/Program Files (x86)/AstraViewerComp/include/astraviewercomp-1"
#    ${<dependency>_INCLUDE_DIRS}
#    )
#
#set(AstraViewerComp_IDL_DIRS
#    "C:/Program Files (x86)/AstraViewerComp/include/astraviewercomp-1/idl")
set(AstraViewerComp_INCLUDE_DIRS
    "C:/Program Files (x86)/AstraViewerComp/include/"
    ${<dependency>_INCLUDE_DIRS}
    )
set(AstraViewerComp_IDL_DIRS
    "C:/Program Files (x86)/AstraViewerComp/include//idl")


if(WIN32)
    set(AstraViewerComp_LIBRARIES
        "C:/Program Files (x86)/AstraViewerComp/components/lib/astraviewercomp.lib"
        ${<dependency>_LIBRARIES}
        )
else(WIN32)
    set(AstraViewerComp_LIBRARIES
        "C:/Program Files (x86)/AstraViewerComp/components/lib/astraviewercomp.dll"
        ${<dependency>_LIBRARIES}
        )
endif(WIN32)

set(AstraViewerComp_DEFINITIONS ${<dependency>_DEFINITIONS})

set(AstraViewerComp_VERSION 1.0.0)
set(AstraViewerComp_VERSION_MAJOR 1)
set(AstraViewerComp_VERSION_MINOR 0)
set(AstraViewerComp_VERSION_REVISION 0)
set(AstraViewerComp_VERSION_CANDIDATE )

