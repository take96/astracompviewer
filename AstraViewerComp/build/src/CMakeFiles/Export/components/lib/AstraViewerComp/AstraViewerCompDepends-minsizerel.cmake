#----------------------------------------------------------------
# Generated CMake target import file for configuration "MinSizeRel".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "AstraViewerComp" for configuration "MinSizeRel"
set_property(TARGET AstraViewerComp APPEND PROPERTY IMPORTED_CONFIGURATIONS MINSIZEREL)
set_target_properties(AstraViewerComp PROPERTIES
  IMPORTED_IMPLIB_MINSIZEREL "${_IMPORT_PREFIX}/components/lib/AstraViewerComp.lib"
  IMPORTED_LINK_INTERFACE_LIBRARIES_MINSIZEREL "RTC112_vc12;coil112_vc12;omniORB421_rt;omniDynamic421_rt;omnithread40_rt;advapi32;ws2_32;mswsock;opencv_core331.lib;opencv_highgui331.lib;opencv_imgproc331.lib;opencv_video331.lib"
  IMPORTED_LOCATION_MINSIZEREL "${_IMPORT_PREFIX}/components/bin/AstraViewerComp.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS AstraViewerComp )
list(APPEND _IMPORT_CHECK_FILES_FOR_AstraViewerComp "${_IMPORT_PREFIX}/components/lib/AstraViewerComp.lib" "${_IMPORT_PREFIX}/components/bin/AstraViewerComp.dll" )

# Import target "AstraViewerCompComp" for configuration "MinSizeRel"
set_property(TARGET AstraViewerCompComp APPEND PROPERTY IMPORTED_CONFIGURATIONS MINSIZEREL)
set_target_properties(AstraViewerCompComp PROPERTIES
  IMPORTED_LOCATION_MINSIZEREL "${_IMPORT_PREFIX}/components/bin/AstraViewerCompComp.exe"
  )

list(APPEND _IMPORT_CHECK_TARGETS AstraViewerCompComp )
list(APPEND _IMPORT_CHECK_FILES_FOR_AstraViewerCompComp "${_IMPORT_PREFIX}/components/bin/AstraViewerCompComp.exe" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
