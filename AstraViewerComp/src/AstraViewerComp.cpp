// -*- C++ -*-
/*!
 * @file  AstraViewerComp.cpp
 * @brief Viewer
 * @date $Date$
 *
 * $Id$
 */

#include "AstraViewerComp.h"

// Module specification
// <rtc-template block="module_spec">
static const char* astraviewercomp_spec[] =
  {
    "implementation_id", "AstraViewerComp",
    "type_name",         "AstraViewerComp",
    "description",       "Viewer",
    "version",           "1.0.0",
    "vendor",            "take",
    "category",          "Category",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.mode", "0",

    // Widget
    "conf.__widget__.mode", "spin",
    // Constraints
    "conf.__constraints__.mode", "0,1,2",

    "conf.__type__.mode", "int",

    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
AstraViewerComp::AstraViewerComp(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_depthIn("depth", m_depth),
    m_redIn("red", m_red),
    m_greenIn("green", m_green),
    m_blueIn("blue", m_blue)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
AstraViewerComp::~AstraViewerComp()
{
}



RTC::ReturnCode_t AstraViewerComp::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("depth", m_depthIn);
  addInPort("red", m_redIn);
  addInPort("green", m_greenIn);
  addInPort("blue", m_blueIn);
  
  // Set OutPort buffer
  
  // Set service provider to Ports
  
  // Set service consumers to Ports
  
  // Set CORBA Service Ports
  
  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("mode", m_mode, "0");
  // </rtc-template>
  
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t AstraViewerComp::onFinalize()
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t AstraViewerComp::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t AstraViewerComp::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t AstraViewerComp::onActivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t AstraViewerComp::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t AstraViewerComp::onExecute(RTC::UniqueId ec_id)
{

	if (m_blueIn.isNew() && m_greenIn.isNew() && m_redIn.isNew()){
		m_blueIn.read();
		m_greenIn.read();
		m_redIn.read();

		if (m_blue.width != BlueBuff.cols || m_blue.height != BlueBuff.rows)
			BlueBuff.create(cv::Size(m_blue.width, m_blue.height), CV_8UC1);
		if (m_green.width != GreenBuff.cols || m_green.height != GreenBuff.rows)
			GreenBuff.create(cv::Size(m_green.width, m_green.height), CV_8UC1);
		if (m_red.width != RedBuff.cols || m_red.height != RedBuff.rows)
			RedBuff.create(cv::Size(m_red.width, m_red.height), CV_8UC1);

		memcpy(BlueBuff.data, (void *)&(m_blue.pixels[0]), m_blue.pixels.length());
		memcpy(GreenBuff.data, (void *)&(m_green.pixels[0]), m_green.pixels.length());
		memcpy(RedBuff.data, (void *)&(m_red.pixels[0]), m_red.pixels.length());

		Color.create(cv::Size(m_red.width, m_red.height), CV_8UC3);



		for (int y = 0; y < Color.rows; ++y){
			for (int x = 0; x < Color.cols; ++x){
				Color.data[y * Color.step + x * Color.elemSize() + 0] =
					BlueBuff.data[y * BlueBuff.step + x * BlueBuff.elemSize()];
				Color.data[y * Color.step + x * Color.elemSize() + 1] =
					GreenBuff.data[y * GreenBuff.step + x * GreenBuff.elemSize()];
				Color.data[y * Color.step + x * Color.elemSize() + 2] =
					RedBuff.data[y * RedBuff.step + x * RedBuff.elemSize()];
			}
		}
		cv::imshow("Color", Color);
	}

	if (m_depthIn.isNew()){
		m_depthIn.read();
		if (m_depth.width != Depth.cols || m_depth.height != Depth.rows)
			Depth.create(cv::Size(m_depth.width, m_depth.height), CV_8UC1);
		memcpy(Depth.data, (void *)&(m_depth.pixels[0]), m_depth.pixels.length());
		cv::imshow("Depth", Depth);
	}
	
	
	cv::waitKey(1);
	return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t AstraViewerComp::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t AstraViewerComp::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t AstraViewerComp::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t AstraViewerComp::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t AstraViewerComp::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{
 
  void AstraViewerCompInit(RTC::Manager* manager)
  {
    coil::Properties profile(astraviewercomp_spec);
    manager->registerFactory(profile,
                             RTC::Create<AstraViewerComp>,
                             RTC::Delete<AstraViewerComp>);
  }
  
};


